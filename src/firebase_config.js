import { initializeApp } from "firebase/app";
import {getAuth} from 'firebase/auth'
import { getFirestore} from 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyA4uZFKyHLsbAiWcbSk39WvAGjbi0oB_zw",
    authDomain: "quiz1-97b7b.firebaseapp.com",
    projectId: "quiz1-97b7b",
    storageBucket: "quiz1-97b7b.appspot.com",
    messagingSenderId: "912577289233",
    appId: "1:912577289233:web:7f739da144c39b452025b0",
    measurementId: "G-RFK3LVLGXN"
  };

  const app = initializeApp(firebaseConfig)
  export const db =getFirestore(app)
  export const auth = getAuth(app)
  