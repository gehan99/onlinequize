
import './App.css';
import { Route, BrowserRouter, Routes } from 'react-router-dom'
import Register from './component/register'
import Login from './component/login'
import ForgotPassword from './component/forgotPassword'
import AddNewpassword from './component/addNewpassword';
import Home from './component/home'
import Quiz from './component/quiz';
// import QuizReasult from './component/qiuizeResult';
import UserHome from './component/userHomepage';
import AddQuestions from './component/teachers/addquestions';
import TeachersHomePage from './component/teachers/homepageTeachers';
import ViewQuestions from './component/teachers/viewquestions';
import NavigationBar from './component/navigationbar'
import Question from './component/question';
import Quize from './component/quize';
import Answer from './component/answer';
import { useAuth0 } from '@auth0/auth0-react'
import Results from './component/results';

function App() {
  const { isAuthenticated, } = useAuth0()
   
  return (
    <div>

      <BrowserRouter>
        <NavigationBar />
        <Routes>
          {isAuthenticated ? <Route path="/user-home" element={<UserHome />}></Route> : false}
          {/* {isAuthenticated ? <Route path="/" element={<Home />}></Route> : false} */}
          <Route path="/" element={<Home />}></Route>
          {isAuthenticated ? <Route path="/login" element={<Login />}></Route>:<Route path="/" element={<Home />}></Route>}

          {isAuthenticated?<Route path="/register" element={<Register />}></Route>:false}
          {isAuthenticated?<Route path="/forgotpassword" element={<ForgotPassword />}></Route>:false}
          {isAuthenticated ? <Route path="/newpassword" element={<AddNewpassword />}></Route> : false}
          {isAuthenticated ? <Route path="/quize" element={<Quiz />}></Route> : false}
          {isAuthenticated ? <Route path="/Results" element={<Results />}></Route> : false}
          {/* {isAuthenticated ? <Route path="/quize-results" element={<QuizReasult />}></Route> : false} */}
          {isAuthenticated ? <Route path="/user-home" element={<UserHome />}></Route> : false}

          {isAuthenticated ? <Route path="/AddQuestions" element={<AddQuestions />}></Route> : false}
          {isAuthenticated ? <Route path="/TeachersHomePage" element={<TeachersHomePage />}></Route> : false}
          {isAuthenticated ? <Route path="/ViewQuestions" element={<ViewQuestions />}></Route> : false}

          {/* smaple */}
         {isAuthenticated?<Route path="/Question" element={<Question />}></Route>:false} 
        {isAuthenticated? <Route path="/Exam_Quize" element={<Quize />}></Route>:false} 
         {isAuthenticated?<Route path="/Answer" element={<Answer />}></Route>:false} 

        </Routes>
      </BrowserRouter>


    </div>
  );
}

export default App;
