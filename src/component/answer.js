import { useContext } from "react"
import { QuizeContext } from "./context/quize"
import { useNavigate } from 'react-router-dom'
import Results from "./results"
// import SubNavBar from "./subnavbar"
const Answer = () => {
    const navigate = useNavigate();
    const [quizeState, dispatch] = useContext(QuizeContext)


    const SubmitedAnswer = () => {
        navigate("/Results")
    }
    return (
        <>

            <div style={{
                position: 'absolute', left: '50%', top: '60%',
                transform: 'translate(-50%, -50%)'
            }}>
                <div className="card shadow-sm " style={{ width: '50rem', backgroundColor: "rgba(200, 200, 200, 0.4)", fontFamily: "inherit", color: "white" }}>
                    <div className="card-body">


                        {/* https://cdni.iconscout.com/illustration/premium/thumb/online-exam-passed-5570196-4638260.png */}


                        <div className="text-center">
                            <button type="submit" className="btn btn-primary"
                                onClick={() => dispatch({ type: "BACK_QUESTIONS" })}>Back to exam</button>
                        </div >
                        <div className="text-center">
                            <img className="card-img-top" src="https://ged.com/wp-content/uploads/GED-Ready-mobile_shadowless.svg" alt="Card image cap"
                                style={{ width: "350px", height: "270px" }} />
                        </div>

                        <div className="text-center" style={{ padding: "2%" }}>
                            <button type="submit" className="btn btn-success" onClick={SubmitedAnswer} >Submit</button>
                        </div>

                    </div>
                </div>

            </div>



        </>
    )
}

export default Answer;