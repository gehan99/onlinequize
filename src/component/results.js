import React, { useEffect, useContext, useState } from "react";
import { json, useNavigate } from 'react-router-dom'
import Answer from './data/answers.json'
import dataset from './data/firebaseData'
import { db } from '../firebase_config'
import { collection, getDocs } from "firebase/firestore";
import { QuizeContext } from "./context/quize"



const Results = () => {
    const [quizeState] = useContext(QuizeContext)

    const navigate = useNavigate();
    const [countdata, setCountData] = useState(0)
    const [CorrectAnswers, setCorrectAnswers] = useState('')
    // const [resultsDetails, setResultsDetails] = useState([])


    // const currentQuestion = quizeState.CorrectAnswerCount


    const answerArry = quizeState.selectedAnswers
    // console.log("arry",quizeState.selectedAnswers)
    localStorage.setItem('items', JSON.stringify(answerArry));


    // console.log("answer->",Answer)
    let finalAnswers = []

    var userResults = localStorage.getItem('items');
    var dataSet = JSON.parse(userResults)

    console.log("userResults----->", dataSet)

    useEffect(() => {
        GetResults()
        GetQuestions()
        console.log("userResults----->", dataSet)
    })

    const GetResults = async () => {
        // const answerCol = await getDocs(collection(db, "answer"));
        const answerCol = collection(db, 'answer');
        const questionSnapshot = await getDocs(answerCol);
        const answers = questionSnapshot.docs.map(doc => doc.data());
        setCorrectAnswers(answers)
        console.log("firebase answers:-------A--------->", answerCol)
    }

    const GetQuestions = async () => {
        const questionCol = collection(db, 'questions');
        const questionSnapshot = await getDocs(questionCol);
        const questions = questionSnapshot.docs.map(doc => doc.data());
        console.log("firebase questions:", questions)
    }


    const CloseButton = () => {
        navigate("/login")
    }
    // console.log("##############",CorrectAnswers[1])

    console.log("selected data********", dataSet[0])
    console.log("corect data********", Answer[0].answer[0])
    console.log("CorrectAnswers", CorrectAnswers.length)



    for (let i = 0; i < CorrectAnswers.length; i++) {
        if (dataSet[i] === Answer[i].answer[0]) {
            finalAnswers[i] = 'Correct'
            console.log("check", finalAnswers)
            // setCorrectMarks(corrctMarks+1)
            // setCount(countdata +1)
        } else {
            finalAnswers[i] = 'False'
        }
    }

    console.log("count", countdata)

    for (let i = 0; i < finalAnswers.length; i++) {
        if (finalAnswers[i] === "Correct") {
            // const val = countdata +1
            // setCountData(val)
            console.log("%%%%%%%%%", countdata + 1)
        }

    }



    console.log("arr----final---->", finalAnswers)
    // console.log("count===========>",corrctMarks)
    const count = {};

    finalAnswers.forEach(element => {
        count[element] = (count[element] || 0) + 1;
    });
    console.log("-ddddddddddddddddddddddddd---->l",count.Correct);
    // setCountData(count.Correct)

    return (
        <>
            <div style={{
                position: 'absolute', left: '50%', top: '65%',
                transform: 'translate(-50%, -50%)'
            }}>

                <div className="card shadow-sm " style={{ width: '30rem', backgroundColor: "rgba(200, 200, 200, 0.4)", fontFamily: "inherit" }}>
                    <div className="card-body">
                        <div className="text-center"><h5>RESULTS</h5></div>
                        <div className="align-middle" style={{ width: "10%", paddingLeft: "33%" }}>
                            <table className="table center">
                                {/* <caption>List of users</caption> */}
                                <thead>
                                    <tr>
                                        <th scope="col-6">Question</th>
                                        <th scope="col-6">Answer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {finalAnswers.map((i, j) => {
                                        return (
                                            <tr>
                                                <td>{j + 1}</td>
                                                <td>{i}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div className="text-center" style={{ color: "green", fontFamily: "monospace" }}>{count.Correct>6?<p>Congratulation !</p>:"good"}</div>
                            <div className="text-center">{count.Correct}/10</div>
                        </div>
                        <div className="text-center" style={{ padding: "2%" }}>
                            <button type="submit" className="btn btn-success" onClick={CloseButton} >Close</button>
                        </div>

                    </div>
                </div>

            </div>
        </>
    )
}

export default Results