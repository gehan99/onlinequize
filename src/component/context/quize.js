import React, { createContext, useReducer } from "react";
import QuestionBank from '../data/questionBank.json'
import CorrectAnswerDB from '../data/answers.json'
// export default dataset
// import dataset from "../data/firebaseData";
// import CorrectAnswer from '../data/correct_answers.json'
// import QAnswers from '../data/answers.json'

const initialState = {
    // questionList,
    // dataset,
    QuestionBank,
    CorrectAnswerDB,
    // CorrectAnswer,
    // QAnswers,
    selectedCurrentAnswer: '',
    CorrectAnswerCount: 0,

    selectedAnswers: [],
    
    CurrentQuestionIndex: 0,
    ShowResults: false,

};

const reducer = (state, action) => {
    console.log("reducer--->", state, action);
    console.log("qs--->",state.QuestionBank[state.CurrentQuestionIndex].answer)
    console.log("action--->",action.selected_answer )
    console.log("CorrectAnswerCount--->",state.CorrectAnswerCount )
    switch (action.type) {
        case "SELECTED_ANSWER": {
            const CorrectAnswerCount = 
            action.selected_answer === state.QuestionBank[state.CurrentQuestionIndex].answer 
            ? state.CorrectAnswerCount+1
            : state.CorrectAnswerCount
            // const CorrectAnswerCount = action.selected_answer === state.QuestionBank[state.CurrentQuestionIndex].answer
            return {
                ...state,
                selectedCurrentAnswer:action.selected_answer,
                CorrectAnswerCount
            }
        }
        case "NEXT_QUESTION": {
            const ShowResults = state.CurrentQuestionIndex === state.QuestionBank.length - 1;
            // const ShowResults = state.CurrentQuestionIndex === state.questionList.length - 1;
            const CurrentQuestionIndex = ShowResults ? state.CurrentQuestionIndex : state.CurrentQuestionIndex + 1
            return {
                ...state,
                CurrentQuestionIndex,
                ShowResults,
            };
        }
        case "BACK_QUESTIONS": {
            // const selectedAnswers = action.selectedAnswers=''
            const ShowResults = state.CurrentQuestionIndex === 0;
            const CurrentQuestionIndex = ShowResults ? state.CurrentQuestionIndex : state.CurrentQuestionIndex - 1
            return {
                ...state,
                CurrentQuestionIndex,
                ShowResults,
                // selectedAnswers,
            };
        };
        case "BACK_QUESTIONS": {
            return initialState + 1;
        };
        default: return state
    }
    // return state
}






export const QuizeContext = createContext()

export const QuizeProvider = ({ children }) => {

    const value = useReducer(reducer, initialState)

    return <QuizeContext.Provider value={value}>{children}</QuizeContext.Provider>
}