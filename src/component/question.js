import { useContext } from "react"
import { QuizeContext } from "./context/quize"
import AnswerCard from "./answercard"
import dataset from "./data/firebaseData"

const Question = ()  => {
    
    const [quizeState, dispatch] = useContext(QuizeContext)
    const currentQuestion = quizeState.QuestionBank[quizeState.CurrentQuestionIndex]
    // console.log("data-->", currentQuestion.question)
    // console.log("ansers-->", quizeState.QuestionBank[quizeState.CurrentQuestionIndex].a)
    // console.log("selected answer",answerText)
    console.log("clicked answers",quizeState.selectedCurrentAnswer)
    console.log("index=====",quizeState.CurrentQuestionIndex)
    console.log("selected--->",quizeState.selectedAnswers[quizeState.CurrentQuestionIndex]= quizeState.selectedCurrentAnswer)
    
    console.log("arry",quizeState.selectedAnswers)
    return (
        <>
            <div className="card shadow-sm " style={{ width: '50rem', backgroundColor: "gainsboro", fontFamily: "inherit", color: "black" }}>
                <div className="card-body">

                    <p className="card-text"></p>
                    <div className="form-group">
                        <p className="h6">{quizeState.CurrentQuestionIndex + 1}. {currentQuestion.question}</p>
                    </div>
                    <br />
                    {quizeState.QuestionBank[quizeState.CurrentQuestionIndex].a.map((answer, index) => (
                        <AnswerCard
                            answerText={answer}
                            key={index}
                            index={index}
                            selectedAnswer={(answerText) => dispatch({ type: "SELECTED_ANSWER", selected_answer: answerText })} />
                    ))}
                    {/* <AnswerCard /> */}
                </div>
            </div>

        </>
    )
}

export default Question