import React, { useState } from "react";
import { createUserWithEmailAndPassword, } from "firebase/auth";
import { auth, db } from '../firebase_config'
import { useNavigate } from 'react-router-dom'
import { doc,setDoc,} from "firebase/firestore";

const Register = () => {
    const navigate = useNavigate();
    const [firstName, setfirstName] = useState("")
    const [lastName, setlastName] = useState("")
    const [mobileNumber, setmobileNumber] = useState("")
    const [registerEmail, setRegisterEmail] = useState("")
    const [registerPassword, setRegisterPassword] = useState("")
    const [registerType, setRegisterType] = useState("")

    const register = async () => {
        try {
            const res = await createUserWithEmailAndPassword(
                auth,
                registerEmail,
                registerPassword,
                registerType
            )
            const user = res.user
            const docRef = doc(db, 'users', user.uid)
            
            await setDoc(docRef, { 
                user: user.uid,
                firstName:firstName,
                lastName:lastName,
                mobile:mobileNumber,
                type: registerType,
                createdAt: Date(),
                email: registerEmail,
                active: "true"
            })
            
            // await addDoc(collection(db, "users",), {
            //     user: user.uid,
            //     type: registerType,
            //     createdAt: Date(),
            //     email: registerEmail
            // })
            console.log("user", user)
            navigate('/')
        } catch (error) {
            console.log(error.message)
        }

    }

    return (

        <>
            <div style={{
                position: 'absolute', left: '50%', top: '60%',
                transform: 'translate(-50%, -50%)'
            }}>
                <div className="card shadow-sm " style={{ width: '30rem' }}>
                    <div className="card-body">
                        <h5 className="card-title">Register</h5>
                        <p className="card-text"></p>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">First Name</label>
                            <input type="text" className="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter Firstname"
                                onChange={(event) => { setfirstName(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Last Name</label>
                            <input type="text" className="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter LastName"
                                onChange={(event) => { setlastName(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Mobile Number</label>
                            <input type="number" className="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter Mobile number"
                                onChange={(event) => { setmobileNumber(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" placeholder="Enter email"
                                onChange={(event) => { setRegisterEmail(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control" id="exampleInputPassword1"
                                placeholder="Password" onChange={(event) => { setRegisterPassword(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Type</label>
                            <input type="password" className="form-control" id="exampleInputPassword1"
                                placeholder="text" onChange={(event) => { setRegisterType(event.target.value) }} />
                        </div>
                        <br />
                        <button type="submit" className="btn btn-primary"
                            onClick={register}>Create</button>
                            

                    </div>
                </div>
            </div>
        </>
    )
}

export default Register