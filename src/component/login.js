import React, { useEffect, useState } from "react";
// import { signInWithEmailAndPassword, signOut, onAuthStateChanged } from "firebase/auth";
// import { auth, db } from '../firebase_config'
import { useNavigate, Link } from 'react-router-dom'
import Userdata from './data/users.json'
import SubNavBar from "./subnavbar";
import { db } from '../firebase_config'
import { collection, getDocs } from "firebase/firestore";
import dataset from './data/firebaseData'


// import { updateDoc, doc } from 'firebase/firestore'



const Login = () => {
    const [LoginEmail, setLoginEmail] = useState("")
    const [LoginPasword, setLoginPasword] = useState("")
    // const [user, setUser] = useState({})
    // useEffect(() => {
    //     onAuthStateChanged(auth, (currentUser) => {
    //         setUser(currentUser)
    //         if (user?.uid !== undefined) {
    //             // updateStatus()
    //         }
    //     })
    // })
    const navigate = useNavigate();
    const[questionDataset, setQuestionDataSet]=useState()
    dataset.N=questionDataset

    useEffect(()=>{
        GetQuestions()
        console.log("----->",dataset.N)

    },[])

const GetQuestions= async()=>{
        const questionCol = collection(db, 'questions');
        const  questionSnapshot = await getDocs(questionCol);
        const questions = questionSnapshot.docs.map(doc => doc.data());
        setQuestionDataSet(questions)
        console.log("firebase questions:",questions)
    }








    // const Login = async () => {
    //     try {
    //         const userData = await signInWithEmailAndPassword(
    //             auth,
    //             LoginEmail,
    //             LoginPasword
    //         )
    //         console.log("user", user)
    //         console.log("logindetailsuser-->", userData)
    //         // onAuthStateChanged()
    //         navigate('/user-home');
    //     } catch (error) {
    //         console.log(error.message)
    //     }
    // }

    // const updateStatus = async () => {

    //     const usercollref = await doc(db, 'users', user?.uid)
    //     await updateDoc(usercollref, {
    //         active: "active"
    //     })
    // }
    // const Logout = async () => {
    //     const examcollref = doc(db, 'users', user?.uid)
    //     await updateDoc(examcollref, {
    //         active: "false"
    //     })
    //     await signOut(auth)
    //     navigate('/')
    // }

    const LoginButton = () => {
        for (var i = 0; i < Userdata.length; i++) {
            if (LoginEmail === Userdata[i].email && LoginPasword === Userdata[i].password) {
                console.log("true")
                localStorage.setItem('type', JSON.stringify(Userdata[i].type));
                navigate('/user-home');
            }
        }
    }
    const homepage = () => {
        navigate('/user-home')
    }
    return (
        <>
            <SubNavBar />
            <div>
                <img className="card-img-top" src="https://media.istockphoto.com/photos/blue-textured-background-picture-id1169630303?b=1&k=20&m=1169630303&s=170667a&w=0&h=fVO3J3fsBjTuukcuSC6k69nUrhHGJWKGifcCGS-X8oE=" alt="Card image cap" style={{
                    position: "fixed",
                    minWidth: "100%",
                    minHeight: "100%",
                    backgroundSize: "cover",
                    backgroundPosition: "center"
                }} />
            </div>
            {/* <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)'
            }}>
                <div className="card shadow-sm " style={{ width: '30rem' }}>
                    <div className="card-body">
                        <h5 className="card-title">SIGN IN</h5>
                        <p className="card-text"></p>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter email"
                                onChange={(event) => { setLoginEmail(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control"
                                id="exampleInputPassword1" placeholder="Password"
                                onChange={(event) => { setLoginPasword(event.target.value) }} 
                                />
                        </div>
                        <br />
                        <div style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={Login} >Login</button>
                        </div>
                        <Link className="nav-link" to='/register'><p className="text-center">Create Your Account</p></Link>
                        <p>login:{user?.email}</p>
                    </div>
                </div>
            </div> */}

            <div style={{
                position: 'absolute', left: '50%', top: '60%',
                transform: 'translate(-50%, -50%)'
            }}>
                <div className="card shadow-sm " style={{ width: '50rem', backgroundColor: "rgba(200, 200, 200, 0.4)", fontFamily: "inherit", color: "white" }}>
                    <div className="card-body">
                        <h6> OOP (Java) </h6>
                        This Java Online Test simulates a real online certification exams.
                        You will be presented Multiple Choice Questions (MCQs) based on Core Java Concepts, where you will be given four options. You will select the best suitable answer for the question and then proceed to the next question without wasting given time.
                        You will get your online test score after finishing the complete test.
                        {/* <h5 className="card-title">SIGN IN</h5>
                        <p className="card-text"></p>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Email address</label>
                            <input type="email" className="form-control"
                                id="exampleInputEmail1" aria-describedby="emailHelp"
                                placeholder="Enter email"
                                onChange={(event) => { setLoginEmail(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Password</label>
                            <input type="password" className="form-control"
                                id="exampleInputPassword1" placeholder="Password"
                                onChange={(event) => { setLoginPasword(event.target.value) }} 
                                />
                        </div> */}
                        {/* <br /> */}
                        {/* <div style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={LoginButton}>Login</button>
                        </div> */}
                        <div style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={homepage}>Start</button>
                        </div>

                        {/* <Link className="nav-link" to='/register'><p className="text-center">Create Your Account</p></Link> */}
                        {/* <p>login:{user?.email}</p> */}
                    </div>
                </div>
                <br />
                <div className="card shadow-sm " style={{ width: '50rem', backgroundColor: "rgba(200, 200, 200, 0.4)", fontFamily: "inherit", color: "white" }}>
                    <div className="card-body">
                        <div className="row" style={{padding:"1%"}}>
                            <div className="col-7">Exam-1 Authentication</div>
                            <div className="col-3"><button type="submit" className="btn btn-warning " >Start</button></div>
                            <div className="col-2">2022/10/21</div>
                        </div>
                        <div className="row" style={{padding:"1%"}}>
                            <div className="col-7">Exam-2 AWS</div>
                            <div className="col-3"><button type="submit" className="btn btn-warning" >Start</button></div>
                            <div className="col-2">2022/10/27</div>
                        </div>
                        <div className="row" style={{padding:"1%"}}>
                            <div className="col-7">Exam-3 Node Js</div>
                            <div className="col-3"><button type="submit" className="btn btn-warning" >Start</button></div>
                            <div className="col-2">2022/11/10</div>
                        </div>
                        <div className="row" style={{padding:"1%"}}>
                            <div className="col-7">Exam-4 React Js</div>
                            <div className="col-3"><button type="submit" className="btn btn-warning" >Start</button></div>
                            <div className="col-2">2022/11/31</div>
                        </div>

                        {/* <div style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={homepage}>Next</button>
                        </div> */}

                    </div>
                </div>



            </div>
            <div>
            </div>

        </>
    )
}

export default Login