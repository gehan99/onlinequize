import React, { useState } from "react";
import {db } from '../../firebase_config'
import { doc,setDoc,} from "firebase/firestore";


const AddQuestions =()=>{
    const[question,setQuestion]= useState("")
    const[answer1,setAnswer1]=useState("")
    const[answer2,setAnswer2]=useState("")
    const[answer3,setAnswer3]=useState("")
    const[answer4,setAnswer4]=useState("")
    const[answer,setAnswer]=useState("")



    const addQuestion = async()=>{
        await setDoc(doc(db, "questions", "LA"), {
            question: question,
            answer1: answer1,
            answer2: answer2,
            answer3:answer3,
            answer4:answer4,
            answer:answer
          });
    }
    
    return(
        <>
        <div style={{
                position: 'absolute', left: '50%', top: '65%',
                transform: 'translate(-50%, -50%)'
            }}>
                <div className="card shadow-sm " style={{ width: '50rem' }}>
                    <div className="card-body">
                        <h5 className="card-title">ADD YOUR QUESTION</h5>
                        <p className="card-text"></p>
                        <div className="form-group">
                            <label htmlFor="exampleInputEmail1">Add Question</label>
                            <input type="text" className="form-control" id="setQuestion"
                                placeholder="Answer"  onChange={(event) => { setQuestion(event.target.value) }} />
                        </div>
                        <br/>
                        
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Answer 1</label>
                            <input type="text" className="form-control" id="setAnswer1"
                                placeholder="Answer"  onChange={(event) => { setAnswer1(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Answer 2</label>
                            <input type="text" className="form-control" id="setAnswer2"
                                placeholder="Answer" onChange={(event) => { setAnswer2(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Answer 3</label>
                            <input type="text" className="form-control" id="exampleInputPassword1"
                                placeholder="Answer" onChange={(event) => { setAnswer3(event.target.value) }} />
                        </div>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Answer 4</label>
                            <input type="text" className="form-control" id="setAnswer4"
                                placeholder="Answer" onChange={(event) => { setAnswer4(event.target.value) }} />
                        </div>
                        <br/>
                        <div className="form-group">
                            <label htmlFor="exampleInputPassword1">Correct answer</label>
                            <input type="text" className="form-control" id="setAnswer"
                                placeholder="Answer" onChange={(event) => { setAnswer(event.target.value) }} />
                        </div>
                        <br />
                        <button type="submit" className="btn btn-primary"
                            onClick={addQuestion}
                            >Add
                            </button>


                    </div>
                </div>
            </div>
        </>
    )
}
export default AddQuestions





// import React from "react";
// import { useNavigate } from 'react-router-dom'

// const Home = () => {
//     const navigate = useNavigate();
//     const login = () => {
//         navigate('/login')
//     }
//     const register = () => {
//         navigate('/register')
//     }
//     return (
//         <>
//             <div style={{background:"danger"}}>
//                 <div style={{
//                     position: 'absolute', left: '50%', top: '50%',
//                     transform: 'translate(-50%, -50%)',
//                 }}>
//                     <div className="text-center" >
//                         <p> ONLINE EXAM</p>
//                     </div>
//                     <div className="row" >
//                         <div className="col" style={{ padding: 3 }}>
//                             <button type="submit" className="btn btn-primary" onClick={login} >Login..</button>
//                         </div>

//                         <div className="col" style={{ padding: 3 }}>
//                             <button type="submit" className="btn btn-primary" onClick={register} >Register</button>
//                         </div>
//                     </div>
//                 </div>
//             </div>
//         </>
//     )
// }

// export default Home