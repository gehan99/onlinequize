import { useContext } from "react"
import { QuizeContext } from "./context/quize"

const AnswerCard = ({ answerText, index, selectedAnswer }) => {
    let myArray = [];
   

    

    // const test = ()=>{
       
            // myArray.push(quizeState.CurrentQuestionIndex)
            // myArray[quizeState.CurrentQuestionIndex]=answerText
    //    console.log("index",quizeState.CurrentQuestionIndex)
        // console.log("arry----->",myArray)
    // }


    const [quizeState] = useContext(QuizeContext)

    const currentQuestion = quizeState.QuestionBank[quizeState.CurrentQuestionIndex]
    // console.log("--->--------------------->", quizeState)
    const letterCounting = ['a', 'b', 'c', 'd']
    // const array = [quizeState.CurrentQuestionIndex]
    // console.log("array--->", array)
    
    // myArray[quizeState.CurrentQuestionIndex]= {answerText}
    // localStorage.setItem('answers',JSON.stringify(myArray) );
    

    return (
        <>
            <div>
                <div className="form-check">
                    <button type="submit" className="btn btn-primary" style={{ paddingRight: "1%" }}
                        onClick={() => selectedAnswer(answerText)}>{letterCounting[index]}</button>
                    <label className="form-check-label" htmlFor="flexRadioDefault1" style={{ padding: "1%" }}>
                        <p className="h6">{answerText}</p>
                    </label>

                    {/* <button type="submit" className="btn btn-primary" style={{ paddingRight: "1%" }} onClick={test}>test
                    </button> */}
                </div>
            </div>

            <br />

        </>
    )
}
export default AnswerCard