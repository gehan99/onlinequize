import React, { useState, useEffect } from "react";
import { onAuthStateChanged, } from "firebase/auth";
import { auth } from '../firebase_config'
import { useNavigate } from 'react-router-dom'
import SubNavBar from "./subnavbar";
// import {doc,getDoc} from "firebase/firestore";


const UserHome = () => {
    const [user, setUser] = useState({})
    const navigate = useNavigate();
    // const docRef = doc(db, "users",user?.uid );
    useEffect(() => {
        onAuthStateChanged(auth, (currentUser) => {
            setUser(currentUser)
        })
    })
    console.log("user->", user?.uid)

    const quizepage = () => {
        navigate('/Exam_Quize')
    }
    const secondPage = () => {
        navigate('/login')
    }
    return (
        <>
            <SubNavBar />
            <div>
                <img className="card-img-top" src="https://media.istockphoto.com/photos/blue-textured-background-picture-id1169630303?b=1&k=20&m=1169630303&s=170667a&w=0&h=fVO3J3fsBjTuukcuSC6k69nUrhHGJWKGifcCGS-X8oE=" alt="Card image cap" style={{
                    position: "fixed",
                    minWidth: "100%",
                    minHeight: "100%",
                    backgroundSize: "cover",
                    backgroundPosition: "center"
                }} />
            </div>

            <div style={{ background: "danger" }}>
                <div style={{
                    position: 'absolute', left: '50%', top: '56%',
                    transform: 'translate(-50%, -50%)',
                }}>
                    <div className="card shadow-sm " style={{ width: '50rem', backgroundColor: "rgba(200, 200, 200, 0.4)", fontFamily: "inherit", color: "white" }}>
                        <div className="card-body">
                            <p> Rules to follow during all online proctored exams</p>
                            <li>You must use a functioning webcam and microphone</li>
                            <li>No cell phones or other secondary devices in the room or test area</li>
                            <li>Your desk/table must be clear or any materials except your test-taking device</li>
                            <li>No talking </li>
                            <li>The testing room must be well-lit and you must be clearly visible</li>
                            <li>No dual screens/monitors</li>
                            <li>Do not leave the camera</li>
                            <li>No use of additional applications or internet</li>
                            <br />

                            <div className="row">
                                <div className="col-1"><div style={{ padding: 3 }}>
                                    <button type="submit" className="btn btn-primary" onClick={secondPage}>Back</button>
                                </div>
                                </div>
                                <div className="col-1">
                                    <div style={{ padding: 3 }}>
                                        <button type="submit" className="btn btn-success" onClick={quizepage} >Start</button>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>



























                    {/* <div className="row" >
                        <div className="text-left" >
                            <p> Rules to follow during all online proctored exams</p>
                            <li>You must use a functioning webcam and microphone</li>
                            <li>No cell phones or other secondary devices in the room or test area</li>
                            <li>Your desk/table must be clear or any materials except your test-taking device</li>
                            <li>No talking </li>
                            <li>The testing room must be well-lit and you must be clearly visible</li>
                            <li>No dual screens/monitors</li>
                            <li>Do not leave the camera</li>
                            <li>No use of additional applications or internet</li>
                        </div>
                    </div> */}
                    {/* <div className="row text-left" >
                        <div className="col" style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={quizepage} >Start</button>
                        </div>

                    </div> */}
                </div>
            </div>
        </>
    )
}
export default UserHome