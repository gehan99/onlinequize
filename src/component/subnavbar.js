import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom"
// import { GrCloudComputer } from "react-icons/gr";
import { AiOutlineHome } from "react-icons/ai";
// import { useNavigate } from 'react-router-dom'
// import { onAuthStateChanged,signOut } from "firebase/auth";
// import { auth ,db} from '../firebase_config'
// import { updateDoc, doc } from 'firebase/firestore'

//auth 0
import { useAuth0 } from '@auth0/auth0-react'
const SubNavBar = () => {
    const {isAuthenticated,user } = useAuth0()
    const navigate = useNavigate();
    const homepage = () => {
        navigate('/')
    }
    
    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-dark bg-secondary shadow-sm" >
                <div className="container-fluid">
                    <a className="navbar-brand" ></a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                {/* <Link className="nav-link active" aria-current="page" to='/' >Main</Link> */}
                                <button type="submit" className="btn btn-secondary" onClick={homepage} ><AiOutlineHome size='2rem' color="white" /></button>
                            </li>
                        </ul>
                    </div>
                    <div className="collapse navbar-collapse nav justify-content-end" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            {isAuthenticated ?
                                <li className="nav-item">
                                    <Link className="nav-link active" aria-current="page" >{user.email}</Link>
                                </li> : false}

                            {/* <button type="submit" className="btn btn-dark" onClick={Logout}>Logout</button> */}
                            <li className="nav-item">
                                {/* <Link className="nav-link" to='/cart'><FaShoppingCart size='2rem' color="white" /></Link> */}
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}
export default SubNavBar;