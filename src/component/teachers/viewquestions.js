import React, { useState, useEffect } from "react";
import { db } from '../../firebase_config'
import { collection,getDocs } from "firebase/firestore";
import QuestionCard from "./questionCard";

const ViewQuestions = () => {
    const [state, setState] = useState([])
    useEffect(() => {
        sample()
    },[])

    const sample = async () => {
            const questionCol = collection(db, 'questions');
            const  questionSnapshot = await getDocs(questionCol);
            const postList = questionSnapshot.docs.map(doc => doc.data());
            setState(postList);
    }

    // console.log("get all docs-->",state)
    // getDocs().then((aa) => {
        // console.log("aa==>", aa)
        // let val = []
        // aa.docs.forEach((doc) => {
            // val.push({ ...doc.data(), id: doc.id })
            // setState({ ...doc.data(), id: doc.id })
    //     })
    //     console.log("2222",state)


    // }).catch(err => {
    //     console.log(err.message)
    // })


    return (
        <>
            {
            state.map((doc) =>(
                
                <div className="col" style={{paddingLeft:"18%",paddingTop:"2%",paddingBottom:"2%"}}>
                <QuestionCard
                id={doc.id}
                question= {doc.question}
                answer1={doc.answer1}
                answer2={doc.answer2}
                answer3={doc.answer3}
                answer4={doc.answer4}
                />
            </div>
            ))
            }
        </>
    )
}
export default ViewQuestions