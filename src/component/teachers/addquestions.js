import React, { useState } from "react";
import { db } from '../../firebase_config'
import { v4 as uuid } from 'uuid';
import { collection, addDoc, Timestamp, } from "firebase/firestore";

const AddQuestions = () => {
    const unique_id = uuid();
    const [question, setQuestion] = useState("")
    const [answer1, setAnswer1] = useState("")
    const [answer2, setAnswer2] = useState("")
    const [answer3, setAnswer3] = useState("")
    const [answer4, setAnswer4] = useState("")
    const [answer, setAnswer] = useState("")

    const [state, setState] = useState(false)
    const [docid, setDocid] = useState()
    
    const fromdataSet = {
        id:unique_id,
        question: question,
        answer1: answer1,
        answer2: answer2,
        answer3: answer3,
        answer4: answer4,
        answers:[answer1,answer2,answer3,answer4],
        // answer: answer,
        date: Timestamp.fromDate(new Date())
    }

    const addQuestion = async () => {
        const datasetval = await addDoc(collection(db, "questions",), fromdataSet)
            .then((value) => {
                // console.log("User Added, id = ", value.id)
                setDocid(value.id)
                console.log("setDocument id-->", unique_id)
                console.log(value)
            }
            ).
            catch((error) => console.log("Failed to add user: $error", error));

        const addanswer = await addDoc(collection(db, "answer",), {
            questionDocId: unique_id,
            answer: [answer],
            date: Timestamp.fromDate(new Date())
        })
        setState(false)

        console.log("addanswer-->", unique_id)
        
    }

    const ddd = async()=>{
        const addanswer = await addDoc(collection(db, "answer",), 
        {
            questionDocId: docid,
            answer: answer,
            date: Timestamp.fromDate(new Date("December 10, 1815"))
        })
    }

    const addnewQuestion = () => {
        setState(true)
    }

    return (
        <>
            {state ?
                <div style={{
                    position: 'absolute', left: '50%', top: '65%',
                    transform: 'translate(-50%, -50%)'
                }}>
                    <div className="card shadow-sm " style={{ width: '50rem' }}>
                        <div className="card-body">
                            <h5 className="card-title">ADD YOUR QUESTION</h5>
                            <p className="card-text"></p>
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Add Question</label>
                                <input type="text" className="form-control" id="setQuestion"
                                    placeholder="Answer" onChange={(event) => { setQuestion(event.target.value) }} required />
                            </div>
                            <br />

                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Answer 1</label>
                                <input type="text" className="form-control" id="setAnswer1"
                                    placeholder="Answer" onChange={(event) => { setAnswer1(event.target.value) }} required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Answer 2</label>
                                <input type="text" className="form-control" id="setAnswer2"
                                    placeholder="Answer" onChange={(event) => { setAnswer2(event.target.value) }} required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Answer 3</label>
                                <input type="text" className="form-control" id="exampleInputPassword1"
                                    placeholder="Answer" onChange={(event) => { setAnswer3(event.target.value) }} required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Answer 4</label>
                                <input type="text" className="form-control" id="setAnswer4"
                                    placeholder="Answer" onChange={(event) => { setAnswer4(event.target.value) }} required />
                            </div>
                            <br />
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Correct answer</label>
                                <input type="text" className="form-control" id="setAnswer"
                                    placeholder="Answer" onChange={(event) => { setAnswer(event.target.value) }} required />
                            </div>
                            <br />
                            <button type="submit" className="btn btn-primary"
                                onClick={addQuestion}>Add
                            </button>


                        </div>
                    </div>
                </div> : <div className="container" style={{ padding: 10 }}>
                    <button type="submit" className="btn btn-primary" onClick={addnewQuestion}>AddQuestion</button>
                </div>
            }
        </>
    )
}
export default AddQuestions