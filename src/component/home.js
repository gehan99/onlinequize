import React from "react";
import { useNavigate } from 'react-router-dom'

const Home = () => {
    const navigate = useNavigate();
    const login = () => {
        navigate('/login')
    }
    const register = () => {
        navigate('/register')
    }
    return (
        <>
            <div>
                <div>
                    <img className="card-img-top" src="https://www.ncbex.org/assets/Uploads/Callouts/Backgrounds/StudyAidsStore-Callout-Background.jpg" alt="Card image cap" style={{
                        position:"fixed",
                        minWidth:"100%",
                        minHeight:"100%",
                        backgroundSize:"cover",
                        backgroundPosition:"center"
                    }}/>
                </div>


                
                <div style={{ background: "danger" }}>
                    <div style={{
                        position: 'absolute', left: '18%', top: '28%',
                        transform: 'translate(-50%, -50%)',
                    }}>
                        <div className="text-center" style={{color:"white",fontFamily:"-moz-initial",fontSize:50}} >
                            <p > ONLINE EXAM</p>
                        </div>
                     
                        <div className="row" >
                        <div className="col" style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={login} >Go..</button>
                        </div>

                        {/* <div className="col" style={{ padding: 3 }}>
                            <button type="submit" className="btn btn-primary" onClick={register} >Register</button>
                        </div> */}
                    </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Home