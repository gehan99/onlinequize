import React, { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom"
import { GrCloudComputer } from "react-icons/gr";
// import { onAuthStateChanged,signOut } from "firebase/auth";
// import { auth ,db} from '../firebase_config'
// import { updateDoc, doc } from 'firebase/firestore'

//auth 0
import { useAuth0 } from '@auth0/auth0-react'





const NavigationBar = () => {
    const { loginWithRedirect, isAuthenticated, logout, user } = useAuth0()
    // const navigate = useNavigate();
    // const [user, setUser] = useState({})
    // const navigate = useNavigate();
    // useEffect(() => {
    //     onAuthStateChanged(auth, (currentUser) => {
    //         setUser(currentUser)
    //     })
    // })

    // console.log("user->", user?.email)

    // const Logout = async () => {
    // const examcollref = doc(db, 'users', user?.uid)
    // await updateDoc(examcollref, {
    //     active: "false"
    // })
    // await signOut(auth)
    // localStorage.clear()
    // navigate('/')
    // }

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-dark " style={{ backgroundColor: 'black' }}>
                <div className="container-fluid">
                    <a className="navbar-brand" ><GrCloudComputer size='2rem' color="white" />Online Exam</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon" />
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            {/* <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" to='/' >Main</Link>
                            </li> */}
                            {/* {isAuthenticated ? <li className="nav-item">
                                <Link className="nav-link" aria-current="page" to='/AddQuestions' >AddQuestions</Link>
                            </li>
                                : <li></li>} */}
                            {/* {isAuthenticated ? <li className="nav-item">
                                <Link className="nav-link" aria-current="page" to='/TeachersHomePage' >TeachersHomePage</Link>
                            </li> : <li></li>} */}

                            {/* {isAuthenticated ? <li className="nav-item">
                                <Link className="nav-link" aria-current="page" to='/ViewQuestions' >ViewQuestions</Link>
                            </li> : false} */}
                            {/* {isAuthenticated?<li className="nav-item">
                                <Link className="nav-link" aria-current="page" to='/user-home' >userHome</Link>
                            </li>:false}                            */}
                        </ul>
                    </div>
                    <div className="collapse navbar-collapse nav justify-content-end" id="navbarNavDropdown">
                        <ul className="navbar-nav">
                            {/* <li className="nav-item">
                                <Link className="nav-link active" aria-current="page" >{user?.email} </Link>
                            </li> */}
                            {isAuthenticated ?
                                <li className="nav-item">
                                    <Link className="nav-link active" aria-current="page" >Hi.. {user.name}</Link>
                                </li> : false}
                            {isAuthenticated ?
                                (<li className="nav-item">
                                    <Link className="nav-link" onClick={() => logout()} >Logout</Link>
                                </li>) : (<li className="nav-item">
                                    <Link className="nav-link" style={{ color: 'red' }} onClick={() => loginWithRedirect()}>Login</Link>
                                </li>)}
                            {/* <button type="submit" className="btn btn-dark" onClick={Logout}>Logout</button> */}
                            <li className="nav-item">
                                {/* <Link className="nav-link" to='/cart'><FaShoppingCart size='2rem' color="white" /></Link> */}
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </>
    )
}
export default NavigationBar;