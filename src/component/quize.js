import { useContext, useEffect, useState } from "react"
import { QuizeContext } from "./context/quize"
import Question from "./question"
import Answer from "./answer"
import SubNavBar from "./subnavbar"
import { useNavigate } from "react-router-dom"

const Quize = () => {
    const [quizeState, dispatch] = useContext(QuizeContext)
    const [seconds, setSeconds] = useState(59)
    const [minutes, setMinutes] = useState(1)
    var timer;
    const navigate = useNavigate();

    useEffect(() => {
        timer = setInterval(() => {
            setSeconds(seconds - 1);
            if (seconds === 1) {
                setMinutes(minutes - 1);
                setSeconds(59);
            }
        }, 1000)
        // if(minutes==0 && seconds===0)
        return () => clearInterval(timer)
    })

    // const GotoNextPage = () => {
    //     navigate('/Results')
    // }
    // console.log("quizeState", quizeState)
    return (
        <>
            {minutes<1? navigate('/Results') : false}
            <SubNavBar />
            <div >
                <div style={{ paddingLeft: "37%", paddingTop: "1%", paddingBottom: "1%", }}>
                    {minutes > 0 ?
                        <div class="card" style={{ width: '20rem', backgroundColor: "blue", color: "white" }}>
                            <div class="card-body">
                                <div className="text-center" style={{ fontFamily: "monospace" }}>{minutes > 0 ? +minutes : "0"}:{seconds > 0 ? (minutes >= 0) ? (seconds) : "0" : false}</div>
                            </div>
                        </div> : <div class="card" style={{ width: '20rem', backgroundColor: "red", color: "white" }}>
                            <div class="card-body">
                                <div className="text-center" style={{ fontFamily: "monospace" }}>{minutes > 0 ? +minutes : "0"}:{seconds > 0 ? (minutes >= 0) ? (seconds) : "0" : false}</div>
                            </div>
                        </div>}

                </div>
                <div className="col" style={{ paddingLeft: "19%", paddingTop: "2%", paddingBottom: "2%" }}>
                    {quizeState.ShowResults && <Answer />}
                    {!quizeState.ShowResults &&
                        <Question />}
                </div>
                <br />

                {!quizeState.ShowResults &&
                    <div className="row text-center" style={{ paddingBottom: "2%" }}>
                        {quizeState.CurrentQuestionIndex === 0 ? true :
                            (<div className="col-2" style={{ padding: "1%" }}>
                                <div>
                                    <button type="submit" className="btn btn-primary" onClick={() => dispatch({ type: "BACK_QUESTIONS" })}>Back</button>
                                </div>
                            </div>)
                        }

                        <div className="col-2" style={{ padding: "1%" }}>
                            <div>
                                <button type="submit" className="btn btn-primary float-right" onClick={() => dispatch({ type: "NEXT_QUESTION" })}>Next</button>
                            </div>
                        </div>


                        <br />
                    </div>
                }

            </div>
        </>
    )
}

export default Quize