import React from "react";

const AddNewpassword =()=>{

    return(
        <>
        <div style={{
                position: 'absolute', left: '50%', top: '50%',
                transform: 'translate(-50%, -50%)'
            }}>
                <div className="card shadow-sm " style={{ width: '30rem' }}>
                    <div className="card-body">
                        <h5 className="card-title">SIGN IN</h5>
                        <p className="card-text"></p>
                        <form>
                            
                            <div className="form-group">
                                <label htmlFor="exampleInputEmail1">Email address</label>
                                <input className="form-control" type="text" placeholder="Readonly input here…" readonly/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="exampleInputPassword1">Confirm Password</label>
                                <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
                            </div>
                            
                            <br/>
                            <button type="submit" className="btn btn-primary">Confirm</button>
                        </form>
                    </div>
                </div>
            </div>
        </>
    )
}
export default AddNewpassword